'''
Created on Jan 5, 2021

@author: wacero
'''
from obspy import UTCDateTime
import get_good_picks


fdsn_server="192.168.1.35"
#fdsn_server="apps.igepn.edu.ec"
fdsn_port= '8080'
network="EC"
station="CHL1"
channel="HHZ"
start_time=UTCDateTime("2020-03-01T00:00:00")
end_time=UTCDateTime("2020-12-31T00:00:00")
##1 DEGREE APROX 111 KM
min_radius=0.0
max_radius= 1.5
min_magnitud=3.0

fdsn_cl=get_good_picks.get_fdsn_client(fdsn_server, fdsn_port)

latitude,longitude=get_good_picks.get_station_metadata(network, station, channel, fdsn_cl)

eventos= get_good_picks.get_events_by_station_location(fdsn_cl, latitude, longitude, start_time, end_time, min_radius, max_radius, min_magnitud)

picks_file=open("%s_picks.txt" %(station),"w")

get_good_picks.write_picks2use(station,eventos,picks_file)
