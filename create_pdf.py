'''
Created on Sep 10, 2020

@author: wacero
'''
import sys,os

from matplotlib import pyplot as plt
import datetime
from obspy.core.utcdatetime import UTCDateTime
from obspy.clients.arclink import Client as clientArclink

from obspy.clients.fdsn  import Client
from obspy.signal import PPSD
from obspy.imaging.cm import pqlx

plt.rcParams["figure.figsize"]=[16,9]

def get_fdsn_client(fdsn_server, fdsn_port):
    
    try:
        return Client("http://%s:%s" %(fdsn_server,fdsn_port))
    except Exception as e:
        print("Error in connect to fdsn server: %s" %str(e))
        


cliente_arclink=clientArclink('wacero',"192.168.131.2",int(18001))


fdsn_server="192.168.1.35"
fdsn_port= '8080'
fdsn_cl=get_fdsn_client(fdsn_server, fdsn_port)


station="PTGL"
channel="HHZ"
#FECHA con buen pick
pdf_plot_folder="%s_%s" %(station,"pdf")
start_time=UTCDateTime("2021-01-01 00:00:00")
end_time=UTCDateTime("2021-08-01 00:00:00")
#start_time=UTCDateTime("2020-08-23 00:52:38") #qnky
#start_time=UTCDateTime("2020-08-06 19:01:44") #pjtf

delta = end_time.datetime - start_time.datetime

if not os.path.isdir("./%s" %pdf_plot_folder):
    try:
        os.makedirs(pdf_plot_folder)
    except Exception as e:
        print("Error in create folder:%s" %str(e))

for i in range(0,delta.days,1):
    
    print(i)
    temp_start=UTCDateTime(start_time+datetime.timedelta(days=i))
    print(temp_start)

    #"""

#sys.exit()

    try:  
        stream=cliente_arclink.get_waveforms("EC",station,"",channel,temp_start , temp_start+86400*1,route=False, compressed=False)
        
        trace=stream[0]
        #trace.plot()
        df = trace.stats.sampling_rate
        
        inventory = fdsn_cl.get_stations(start_time,network="EC",sta=station,loc="",channel="*Z",level="response")
        
        ppsd = PPSD(trace.stats,metadata=inventory)
        
        ppsd.add(trace)
        
        print(ppsd.times_processed)
        
        print(len(ppsd.times_processed))
        
        resp=ppsd._get_response(trace)
        print("CTM: %s" %resp)
        #for i in resp:
        #    print(i, abs(i))
        ppsd.plot(filename="./%s/%s_%s_pdf.png" %(pdf_plot_folder,station,temp_start.strftime("%Y-%m-%d")),cmap=pqlx)
        #"""

        #ppsd.plot(filename="./%s/%s_%s_pdf_acumul.png" %(pdf_plot_folder,station,temp_start.strftime("%Y-%m-%d")),cmap=pqlx, cumulative=True)
    except Exception as e:
        print("Error in create_pdf: %s" %str(e))
"""
#print(trace)
#Filtro sc3 x defecto 4 a 20, 4 orden
N=int(10*df)
##moving average con obspy
#trace.data=util.smooth(trace.data,int(10*df))
##moving average con convolucion
trace.data=np.convolve(trace.data,np.ones((N,))/N,mode="valid")
#trace.detrend('linear')
trace.data=filter.bandpass(trace.data,1, 3, 100, 4)
trace.plot()
#PARAM SC3 local_100 0.2, 10, trigon 3, trigoff 1.5 
cft=recursive_sta_lta_py(trace.data,nsta=int(2.4*df),nlta=int(70*df))
plot_trigger(trace,cft,4.5,1.5)
print(len(trigger_onset(cft,3.0,1.5)))
"""
