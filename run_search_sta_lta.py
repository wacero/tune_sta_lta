'''
Created on Jan 5, 2021

@author: wacero
'''

from obspy.core.utcdatetime import UTCDateTime
from obspy.clients.arclink import Client as clientArclink
import search_sta_lta_values 
"""
Filtro sc3 x defecto 4 a 20, 4 orden
PARAM SC3 local_100 0.2, 10, 
trigon 3, trigoff 1.5

scautopick,teleseismic "RMHP(10)>>ITAPER(30)>>BW(4,0.7,2)>>STALTA(2,80)"
scautopick,local       "RMHP(10)>>ITAPER(30)>>BW(4,4,20)>>STALTA(0.2,10)"
"""
#FECHA con buen pick
eventid="igepn2021bmas"
event_datetime="2021-11-02T11:26:58"
inicio=UTCDateTime(event_datetime)
network="EC"
station="BMAS"
channel="HHZ"
location=""
windows_size=60

run_mode=0
Fa=0.5
Fb=40
pick_limit=10
pick_tolerance=10 #seconds


#sys.exit(0)
###NO EMPEZAR CON LOS VALORES 0.2 Y 10 SOBRECARGA A GNOME 


#sta_min=0.7
#sta_min=2.2
sta_min=0.5
sta_max=2.5
sta_step=0.5

#lta_min=10
lta_min=5
#lta_min=60
lta_max=30
lta_step=5

trigon_min=3.0
trigon_max=15
trigon_step=0.5
##NO VARIAR EL TRIGOFF
##NO GENERA VARIACIONES
#trigoff_min=1.5
#trigoff_max=1.5
trigoff_min=1.5
trigoff_max=2.0
trigoff_step=0.5



cliente_arclink=clientArclink('wacero',"192.168.131.2",18001)
stream=cliente_arclink.get_waveforms(network,station,location,channel,inicio -windows_size, inicio+windows_size,route=False, compressed=False)

trace1=search_sta_lta_values.preprocess_stream(stream, Fa, Fb)

pick_plot_folder=search_sta_lta_values.create_pick_plot_folder(eventid, event_datetime, station, Fa, Fb)

search_sta_lta_values.search_plot_sta_lta(trace1,sta_min, lta_min, trigon_min, trigoff_min, sta_max, lta_max, trigon_max, trigoff_max, 
                    sta_step, lta_step, trigon_step, trigoff_step, pick_plot_folder, run_mode,
                     windows_size,pick_tolerance,pick_limit)




