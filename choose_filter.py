'''
Created on Dec 11, 2020

@author: wacero
'''

from obspy.clients.arclink import Client as clientArclink

from obspy.signal import filter
from obspy import UTCDateTime
from matplotlib import pyplot as plt 
import os
##LEER EL ARCHIVO CREADO POR GET_GOOD_PICKS


#plt.rcParams["figure.figsize"]=[16,9]
station="CHL1"
location=""
windows_size=600

filters=[(0.01,0.1),(0.1,1),(1,10),(0.01,10),(0.1,10),(0.4,1),(0.7,2),(1,3),(2,4)]
    
cliente_arclink=clientArclink('wacero',"192.168.131.2",18001)

picks_file="./%s_picks_simple.txt" %station
picks=open(picks_file)
picks_lines=picks.readlines()

folder_name="%s_picks" %(station)

if not os.path.isdir("./%s" %folder_name):
    try:
        os.makedirs(folder_name)
    except Exception as e:
        print("Error in create folder") 

for line in picks_lines:
    eventid,mag,network,station,channel,pick_date,julian_day=line.split(",")
    pick_time=UTCDateTime(pick_date)
    
    stream=cliente_arclink.get_waveforms(network,station,location,channel,pick_time -windows_size, pick_time + windows_size,route=False, compressed=False)
    stream.merge(method=1, fill_value="interpolate",interpolation_samples=0)
    trace=stream[0]
    df = trace.stats.sampling_rate
    print(trace)
    trace.detrend('linear')
    trace.plot(outfile='./%s/%s_%s_%s.png' %(folder_name,eventid,station,channel))
    trace_original=trace.copy()
    for f in filters:
        trace_temp=trace.copy()
        f1=f[0]
        f2=f[1]
    
        #trace.detrend('linear')
        trace_temp.data=filter.bandpass(trace_temp.data,f1, f2, df, 4)
        trace_temp.taper(type="cosine",max_percentage=0.5,max_length=30)        
        trace_temp.plot(outfile='./%s/%s_%s_%s_%s_%s.png' %(folder_name,eventid,station,channel,f1,f2))
        
        plt.close("all") 

