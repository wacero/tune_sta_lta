'''
Created on Sep 29, 2020

@author: wacero
'''

"""
INPUT: nombre estacion 
SALIDA: event_id y datetime de picks correctos
"""

from obspy.clients.fdsn  import Client


def get_fdsn_client(fdsn_server, fdsn_port):
    
    try:
        return Client("http://%s:%s" %(fdsn_server,fdsn_port))
    except Exception as e:
        print("Error in connect to fdsn server: %s" %str(e))
        

def get_station_metadata(network,station,channel,fdsn_client):
    
    try:
        station_metadata=fdsn_client.get_stations(network=network,station=station,channel=channel,level="channel",format=None)
        return station_metadata[0][0][0].latitude, station_metadata[0][0][0].longitude 
        
    except Exception as e:
        print("Error in get_station_metadata:%s" %str(e))


def get_events_by_station_location(fdsn_client,latitude,longitude,start_time,end_time,min_radius,max_radius,min_magnitud):
    
    try:
        return fdsn_client.get_events(orderby="time-asc",starttime=start_time,endtime=end_time,latitude='%s'%latitude,
                                       longitude='%s'%longitude,minradius=min_radius,maxradius=max_radius,minmagnitude=min_magnitud,includearrivals=True,includeallorigins=True)
        
    except Exception as e:
        print("Error in get_events_by_station_location: %s" %str(e))

def write_picks2use(station,events,picks_file):

    for event in events:
    
        if event.preferred_origin()['evaluation_mode'] == "manual":
            #print("###%s####,%s" %(i,event['resource_id']))
            eventid=str(event['resource_id'])[-13:]
            arrivos = event.preferred_origin().arrivals
            picos=event.picks       
            pick_list=[]
            for arrivo in arrivos:
                for pico in picos:
                    if pico['resource_id']==arrivo['pick_id']:
                        pick_list.append("%s" %pico['waveform_id'])
                        
            pick_set=set(pick_list)
            
            for pick in pick_set:
                if station in pick:
                    for pick_object in picos:
                        if station in str(pick_object['resource_id']):
                            
                            network=pick_object['waveform_id']['network_code']
                            station=pick_object['waveform_id']['station_code']
                            channel=pick_object['waveform_id']['channel_code']
                            if event.magnitudes:
                                magnitude=event.preferred_magnitude() or event.magnitudes[0]
                                magnitude_value=round(magnitude.mag,2)
                            else:
                                magnitude_value=0     
                            
                            print("%s,%s,%s,%s,%s,%s,%s" %(eventid, magnitude_value,network,station,channel,pick_object['time'],pick_object['time'].julday))
                            picks_file.write("%s,%s,%s,%s,%s,%s,%s\n" %(eventid, magnitude_value,network,station,channel,pick_object['time'],pick_object['time'].julday))
                            break
    picks_file.close()
    


