'''
Created on Sep 10, 2020

@author: wacero
'''
import sys

from matplotlib import pyplot as plt
import numpy as np
from obspy import read
from obspy.signal.trigger import plot_trigger, trigger_onset
from obspy.signal.trigger import classic_sta_lta, classic_sta_lta_py, recursive_sta_lta_py
from obspy.signal import filter 
from obspy.signal import util
from obspy.core.utcdatetime import UTCDateTime
from obspy.clients.arclink import Client as clientArclink

from obspy.clients.fdsn  import Client
from obspy.signal import PPSD
from obspy.imaging.cm import pqlx

plt.rcParams["figure.figsize"]=[16,9]

def get_fdsn_client(fdsn_server, fdsn_port):
    
    try:
        return Client("http://%s:%s" %(fdsn_server,fdsn_port))
    except Exception as e:
        print("Error in connect to fdsn server: %s" %str(e))
        


cliente_arclink=clientArclink('wacero',"192.168.131.2",int(18001))


fdsn_server="192.168.1.35"
fdsn_port= '8080'
fdsn_cl=get_fdsn_client(fdsn_server, fdsn_port)


station="ANTI"
channel="SHZ"
#FECHA con buen pick
inicio=UTCDateTime("2020-12-01 00:00:00")
#inicio=UTCDateTime("2020-08-23 00:52:38") #qnky
#inicio=UTCDateTime("2020-08-06 19:01:44") #pjtf



def get_station_metadata(network,station,channel,fdsn_client):
    
    try:
        station_metadata=fdsn_client.get_stations(network=network,station=station,channel=channel,level="channel",format=None)
        return station_metadata[0][0][0].latitude, station_metadata[0][0][0].longitude 
        
    except Exception as e:
        print("Error in get_station_metadata:%s" %str(e))


inventory = fdsn_cl.get_stations(inicio,network="EC",sta="*",loc="",channel="BH*",level="response")

print(inventory.networks)

for station in inventory.networks[0].stations:
    print("EC, %s,%s*,%s,%s,%s" %(station.code,station.channels[0].code[:-1],station.longitude,station.latitude,station.elevation))

"""
#print(trace)
#Filtro sc3 x defecto 4 a 20, 4 orden
N=int(10*df)
##moving average con obspy
#trace.data=util.smooth(trace.data,int(10*df))
##moving average con convolucion
trace.data=np.convolve(trace.data,np.ones((N,))/N,mode="valid")
#trace.detrend('linear')
trace.data=filter.bandpass(trace.data,1, 3, 100, 4)
trace.plot()
#PARAM SC3 local_100 0.2, 10, trigon 3, trigoff 1.5 
cft=recursive_sta_lta_py(trace.data,nsta=int(2.4*df),nlta=int(70*df))
plot_trigger(trace,cft,4.5,1.5)
print(len(trigger_onset(cft,3.0,1.5)))
"""