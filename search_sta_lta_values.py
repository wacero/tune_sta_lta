'''
Created on Sep 10, 2020

@author: wacero
'''
from obspy import read
import sys,os
from obspy.signal import util
import numpy as np
from matplotlib import pyplot as plt
from obspy.signal.trigger import classic_sta_lta, classic_sta_lta_py, recursive_sta_lta_py, plot_trigger, trigger_onset
from obspy.signal import filter 




def create_pick_plot_folder(eventid,event_datetime,station,Fa,Fb):

    pick_plot_folder="%s_%s_%s_%s_%s" %(eventid,event_datetime,station,Fa,Fb)
    
    if not os.path.isdir("./%s" %pick_plot_folder):
        try:
            os.makedirs(pick_plot_folder)
        except Exception as e:
            print("Error in create folder:%s" %str(e))

    return pick_plot_folder



def search_plot_sta_lta(trace,sta_min,lta_min,trigon_min,trigoff_min,sta_max,lta_max,trigon_max,trigoff_max,
                        sta_step,lta_step,trigon_step,trigoff_step,pick_plot_folder,run_mode,
                        windows_size,pick_tolerance,pick_limit):
    """
    This function iterates over sta, lta, trigon, trigoff values searching the ones
    that generates less picks
    run_mode: 0 plot picks of the best parameters
    run_mode: 1 plot all picks
    """
    trace.plot(outfile="./%s/%s.png" %(pick_plot_folder,trace.stats['station']))
    sampling_rate = trace.stats.sampling_rate
    sta = sta_min
    
    while sta <= sta_max:
        lta = lta_min
        while lta <= lta_max:
            trigon=trigon_min
            while trigon<= trigon_max:
                trigoff=trigoff_min            
                while trigoff <= trigoff_max:
                    
                    sta=round(sta,2)
                    lta=round(lta,2)
                    trigon=round(trigon,2)
                    trigoff=round(trigoff,2)
                    
                    #cft=recursive_sta_lta_py(trace.data,nsta=int(sta*sampling_rate),nlta=int(lta*sampling_rate))
                    cft=classic_sta_lta(trace.data,nsta=int(sta*sampling_rate),nlta=int(lta*sampling_rate))
                    '''Create an array with triggers starttime'''
                    on_off=trigger_onset(cft, trigon, trigoff)
                    triggers_on=[]
                    for time in on_off:
                        triggers_on.append(time[0])
                    
                    if run_mode==0:
                        
                        for pick_on in triggers_on:
                            if windows_size - pick_tolerance < pick_on/sampling_rate and pick_on/sampling_rate< windows_size + pick_tolerance :
                                if len(triggers_on)<=pick_limit:
                                    
                                    print("good param")
                                    print(sta,lta,trigon,trigoff)
                                    print(triggers_on)
                                    plot_trigger(trace,cft,round(trigon,2),round(trigoff,2),show=False)
                                    plt.text(10,10,"#pick: %s sta:%0.2f lta:%s trigon %0.2f trigoff %0.2f" %(len(triggers_on), sta,lta,trigon,trigoff))
                                    plt.savefig("./%s/%s_%0.2f_%0.2f_%0.2f_%0.2f.png" %(pick_plot_folder,len(triggers_on),sta,lta,trigon,trigoff))
                                    plt.close("all")
                    
                    elif run_mode==1:
                        
                        print(sta,lta,trigon,trigoff)
                        plot_trigger(trace,cft,round(trigon,2),round(trigoff,2),show=False)
                        plt.text(10,10,"sta:%0.2f lta:%s trigon %0.2f trigoff %0.2f" %(sta,lta,trigon,trigoff))
                        plt.savefig("./%s/%0.2f_%0.2f_%0.2f_%0.2f.png" %(pick_plot_folder,sta,lta,trigon,trigoff))
                        plt.close("all")
                    trigoff +=trigoff_step
                    
                trigon +=trigon_step
                
            lta +=lta_step
            
        sta += sta_step
    


def preprocess_stream(stream,Fa,Fb):
    
    stream.merge(method=1, fill_value="interpolate",interpolation_samples=0)
    trace=stream[0]
    print(trace)
    sampling_rate = trace.stats.sampling_rate
    print(sampling_rate)
    
    
    '''
    NO USAR MOVING AVERAGE porque mueve en 10 segundos el centro de la senal
    N=int(10*sampling_rate)
    #moving average con obspy
    trace.data=util.smooth(trace.data,int(10*sampling_rate))
    ##moving average con convolucion
    trace.data=np.convolve(trace.data,np.ones((N,))/N,mode="valid")
    '''
    #N=int(10*sampling_rate)
    #trace.data=np.convolve(trace.data,np.ones((N,))/N,mode="valid")
    trace.detrend('linear')
    trace.data=filter.bandpass(trace.data,Fa, Fb, sampling_rate, 4)
    trace.taper(type="cosine",max_percentage=0.5,max_length=30)
    print(trace)
    return trace






